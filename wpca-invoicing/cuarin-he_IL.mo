��    �      <      \      \     ]     n     |     �     �     �     �     �  
   �     �     �     �  a   �  q   `	     �	     �	  	   �	     �	     �	     
     
  ]   (
     �
     �
  
   �
  
   �
     �
     �
     �
     �
     �
     �
               (     =     Z  "   c     �     �     �     �     �     �     �     �     �  #   �                1     B     S     q     �     �     �     �          -     M     a     p          �     �     �     �     �     �     �     �     
          #     >     Y     y     �     �     �  
   �     �     �     �     �     �                    %     *     2     ?     H  ,   X     �     �  '   �     �     �     �     �     �     �               -     ;     [     i     ~     �     �     �     �     �  &        >     [  *   u  2   �  (   �  >   �  '   ;  "   c  $   �  &   �  ,   �  +   �  q   +  ]  �     �          '     C     L     `     p     �     �  %   �     �     �  �   �  �   �          2     H     Q     X     q     z  e   �  "   �          (     8     D     M     `     }     �     �     �     �     �  ,        2  '   ;     c     w     �     �     �     �     �     �     �     �  $   �           .  "   O     r     {  
   �     �     �     �     �  
   �  '   �     �     �           2     S     m     �     �     �     �     �     �       '   &  #   N  (   r  !   �     �     �     �     �     
       "   )     L     f     v     �     �     �  
   �     �     �     �     �  
   �  (      -   0      ^   	   g      q   #   z      �   .   �      �   #   �      !  &   #!     J!     d!      t!     �!     �!     �!     �!     �!  )   "     ."     F"  )   \"  9   �"  &   �"  G   �"  "   /#     R#     l#      �#  0   �#  &   �#  �   $   %2$s - %1$.2f %% (excl. taxes) Access invoices Add Add a description Added on %s After taxes Amount Amount due Apply predefined rate Assign statuses Assigned to Automatically set invoice to this status when payments total amount corresponds to invoice amount Automatically set invoice to this status when payments total amount does not correspond to invoice amount anymore Before taxes Billing address Cancelled Clear Clear the address Client Closed statuses Consider that the invoice is not due by the client anymore when it has any of these statuses. Create/Edit invoices Created Created by Created on Currency Current value:  Default currency Default status Default text Delete invoices Delete statuses Discount Discount - %1$.2f %% Do not update invoice status Download Download the invoice as a PDF file Due date Due on %1$s Edit statuses Fixed amount From From:  General info. Information Invoice Invoice PDF filename prefixinvoice Invoice billing address Invoice currency Invoice discount Invoice due date Invoice item unit pluraldays Invoice item unit pluralhours Invoice item unit pluraltimes Invoice item unit pluralunits Invoice item unit singularday Invoice item unit singularhour Invoice item unit singulartime Invoice item unit singularunit Invoice items total Invoice notice Invoice number Invoice numbering Invoice payment mode Invoice properties Invoice status Invoice taxes Invoice total Invoice totals Invoice: Invoice: %1$s Invoices Invoices - Home Invoices published by %1$s Invoices published in %1$s Invoices published in %2$s %1$s Invoices with status: %1$s Invoices you published Item Item description Item title Items Items total List all invoices My invoices No discount No taxes Open Page %1$s/%2$s Paid Payment Payment mode Quantity Recent Invoices Slug for URL to download an invoicedownload Status Subtotal (excl. taxes) There are no invoices in that category. To To:  Total Total price (excl. taxes) Totals Unit price (excl. taxes) View all View any invoice View invoices You currently have no invoices. [Default: %s] cuar_invoiceAdd New cuar_invoiceAdd New Invoice cuar_invoiceEdit Invoice cuar_invoiceInvoice cuar_invoiceInvoices cuar_invoiceNew Invoice cuar_invoiceNo invoice found cuar_invoiceNo invoice found in Trash cuar_invoiceSearch Invoices cuar_invoiceView Invoice cuar_invoice_statusAdd New Invoice Status cuar_invoice_statusAdd or remove invoice statuses cuar_invoice_statusAll Invoice Statuses cuar_invoice_statusChoose from the most used invoice statuses cuar_invoice_statusEdit Invoice Status cuar_invoice_statusInvoice Status cuar_invoice_statusInvoice Statuses cuar_invoice_statusNew Invoice Status cuar_invoice_statusPopular Invoice Statuses cuar_invoice_statusSearch Invoice Statuses {{number}} will be replaced by the invoice number. This allows you to prefix/suffix the displayed invoice number. Project-Id-Version: WP Customer Area - Invoicing 2.5.0
Report-Msgid-Bugs-To: http://github.com/marvinlabs/customer-area-build-environment/issues
POT-Creation-Date: 2018-04-26 14:24:10+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-08-11 10:11+0000
Last-Translator: ויז׳ן אנד ביונד <yoni@shtik.co.il>
Language-Team: Hebrew
Language: he_IL
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Country: United States
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-Basepath: ../
X-Poedit-SearchPath-0: .
X-Poedit-Bookmarks: 
X-Textdomain-Support: yes
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.0; wp-5.2.2 %2$s - %1$.2f %% (לא כולל מיסים) גישה לחשבוניות הוסף הוסף תיאור נוסף ב- %s אחרי מיסים כמות הסכום צפוי החל שיעור מוגדר מראש הקצה סטטוסים הוקצה ל הגדר אוטומטית חשבונית למצב זה כאשר הסכום הכולל של התשלומים מתאים לסכום החשבונית הגדר אוטומטית חשבונית למצב זה כאשר הסכום הכולל בתשלומים אינו תואם יותר לסכום החשבונית לפני מיסים כתובת לחיוב בוטל נקה נקה את הכתובת לקוח סטטוסים סגורים קחו בחשבון שהלקוח לא נפרע מהלקוח כאשר יש לו סטטוסים אלה. צור / ערוך חשבוניות נוצר נוצר ע״י נוצר ב מטבע ערך נוכחי: מטבע ברירת מחדל סטטוס ברירת מחדל טקסט ברירת מחדל מחק חשבוניות מחק סטטוסים הנחה הנחה - %1$.2f %% אל תעדכן את מצב החשבונית הורד הורד את הקבלה כקובץ PDF תאריך הגשה פירעון ב %1$s ערוך סטטוסים סכום קבוע מ מ: מידע כללי. מידע חשבונית קבלה כתובת לחיוב חשבונית מטבע חשבונית הנחה עבור חשבונית תאריך יעד לחשבונית ימים שעות זמנים יחידות יום שעה זמן יחידה פריטי חשבונית בסך הכל הודעת חשבונית מספר חשבונית מספור חשבוניות מצב תשלום חשבונית נכסי חשבוניות סטטוס חשבונית מיסי חשבוניות סה"כ חשבונית סיכומי חשבוניות חשבונית: חשבונית: %1$s חשבוניות חשבוניות - בית חשבוניות נוצרו ע״י %1$s חשבוניות נוצרו ב %1$s חשבוניות נוצרו ב %2$s %1$s סטטוס חשבוניות: %1$s חשבוניות שיצרת פריט תיאור פריט כותרת פריט פריטים סך פריטים רשימת כל החשבוניות החשבוניות שלי ללא הנחה ללא מיסים פתוח עמוד %1$s/%2$s שולם תשלום מצב תשלום כמות חשבוניות אחרונות הורד סטטוס תת-חלקי (לא כולל מיסים) אין חשבוניות בקטגוריה זו עבור עבור: סה״כ מחיר כולל (ללא מסים) סך הכל מחיר יחידה (לא כולל מיסים) הצג הכל הצג את כל החשבוניות הצג חשבוניות כרגע אין לך חשבוניות. [ברירת מחדל: %s] הוסף חדש הוסף חשבונית חדשה ערוך חשבונית חשבונית חשבוניות חשבונית חדשה לא נמצאה חשבונית חשבונית לא נמצאה באשפה חפש חשבוניות הצג חשבונית הוסף סטטוס חשבונית חדש הוסף או הסר סטטוסים של חשבוניות כל הסטטוסים לחשבונית בחר מבין סטטוסי החשבונית הנפוצים ביותר ערוך סטטוס חשבונית סטטוס חשבונית סטטוסים לחשבונית סטטוס חשבונית חדש סטטוסים לחשבונית פופולרית חפש סטטוסים לחשבונית {{number}} יוחלף במספר החשבונית. זה מאפשר לך קידומת / סיומת של מספר החשבונית המוצג. 