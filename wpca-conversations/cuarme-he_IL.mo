��    :      �      �      �     �     �     �  1   �     !     7     =     K     `     t     �  "   �     �     �            
   ,     7     <     M     b     o     w     |     �     �  	   �     �  !   �     �  	   �       "        .     J     d     j     s     �     �     �     �  $   �  "   �  &        7  &   Q     x     �  &   �  $   �           #  )   D  &   n  #   �     �  a  �     8     S  
   q  A   |     �     �  
   �     �     �  !        2  "   P     s     �     �     �     �  
   �     �     
       
   .     9     B  
   X     c     w     �  ,   �     �     �     �     �  4     .   G     v          �     �     �     �  0   �      �  0     .   L     {     �     �     �     �     �  
   �     �  #        +     =     O   %1$s replied on %2$s %d reply %d replies Archives Are you sure that you want to delete this reply?  Back to conversations Close Conversations Conversations - Home Conversations - New Conversations started by %1$s Conversations started in %1$s Conversations started in %2$s %1$s Conversations under %1$s Conversations you started Create more Create/Edit conversations Created By Date Delete any reply Delete conversations Delete reply Details Done List all conversations Message My conversations Next Step No replies yet Page to create new conversations. Recent Conversations Recipient Send The conversation has been started. The message cannot be empty The topic cannot be empty Topic View all View any conversation View conversations View it With You cannot send an empty reply You currently have no conversations. You must select at least one owner You must select at least one recipient cuar_conversationAdd New cuar_conversationAdd New Conversation cuar_conversationAdd New Reply cuar_conversationConversation cuar_conversationConversation Replies cuar_conversationConversation Reply cuar_conversationConversations cuar_conversationNo reply found cuar_conversationNo reply found in Trash cuar_conversationSearch Conversations cuar_conversationView Conversation cuar_conversationView Reply Project-Id-Version: WP Customer Area - Conversations 4.3.1
Report-Msgid-Bugs-To: http://github.com/marvinlabs/customer-area-build-environment/issues
POT-Creation-Date: 2018-12-14 12:13:07+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-08-10 10:10+0000
Last-Translator: ויז׳ן אנד ביונד <yoni@shtik.co.il>
Language-Team: Hebrew
Language: he_IL
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Country: United States
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-Basepath: ../
X-Poedit-SearchPath-0: .
X-Poedit-Bookmarks: 
X-Textdomain-Support: yes
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.0; wp-5.2.2 %1$s הגיב/ה על %2$s תגובה %d %d תגובות ארכיב האם אתה בטוח שברצונך למחוק תשובה זו? חזרה לשיחות סגור שיחות שיחות - בית שיחות - חדש שיחות שהחלו ע״י %1$s שיחות שהחלו ב %1$s שיחות שהחלו ב %2$s %1$s שיחות תחת %1$s שיחות שהתחלת צור עוד שיחות צור / ערוך שיחות נוצר ע״י תאריך מחק כל תשובה מחק שיחות מחק תשובה פרטים בוצע רשימת שיחות הודעה השיחות שלי הצעד הבא אין עדיין תשובות עמוד ליצירת שיחות חדשות. שיחות אחרונות נמען שלח השיחה החלה. ההודעה אינה יכולה להיות ריקה הנושא אינו יכול להיות ריק נושא הצג הכל הצג כל שיחה הצג שיחות הצג שיחה עם אינך יכול לשלוח תשובה ריקה כרגע אין לך שיחות. עליך לבחור בעלים אחד לפחות עליך לבחור לפחות נמען אחד הוסף הוסף שיחה הוסף תשובה שיחה תשובות לשיחה תגובה לשיחה שיחות לא נמצאה תשובה לא נמצאה תשובה בזבל חפש שיחות צפה בשיחה צפה בתגובה 