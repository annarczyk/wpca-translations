��                                     '     5     >     F     R     ^     j     r     w     �  	   �  	   �     �     �     �     �               7     Q     W     ^     j     v  �   ~  8        A  "   \  k       �               3     B     K     [     v  
   �     �  "   �     �     �     �  
   �  .   �     )	     >	     U	     j	  4   �	  
   �	     �	     �	     �	     �	  �   �	  ^   �
  "   �
  0      Attachments Back to files Back to pages Category Content Create file Create more Create page Details Done Edit attachments File details Next Step Next step Owner The content cannot be empty The file has been created. The file has been updated. The page has been created. The page has been updated. The title cannot be empty Title Update Update file Update page View it You are not allowed to pick an owner and there are no default owners for this type of content. Please contact your website administrator! You must save the post before you can upload attachments You must select a category You must select at least one owner Project-Id-Version: WP Customer Area - Front-office publishing 4.1.1
Report-Msgid-Bugs-To: http://github.com/marvinlabs/customer-area-build-environment/issues
POT-Creation-Date: 2018-12-14 12:13:07+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-08-11 10:03+0000
Last-Translator: ויז׳ן אנד ביונד <yoni@shtik.co.il>
Language-Team: Hebrew
Language: he_IL
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Country: United States
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-Basepath: ../
X-Poedit-SearchPath-0: .
X-Poedit-Bookmarks: 
X-Textdomain-Support: yes
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.0; wp-5.2.2 קבצים מצורפים חזרה לקבצים חזרה לעמודים קטגוריה תוכן צור קובץ צור עוד עמודים צור עמוד פרטים בוצע ערוך קבצים מצורפים פרטי קובץ הצעד הבא הצעד הבא בעלים התוכן אינו יכול להיות ריק הקובץ נוצר. הקובץ עודכן. העמוד נוצר. העמוד עודכן. הכותרת אינה יכולה להיות ריקה כותרת עדכן עדכן קובץ עדכן עמוד הצג עמוד אינך מורשה לבחור בעלים ואין בעלי ברירת מחדל לסוג תוכן זה. אנא צור קשר עם מנהל האתר שלך! עליך לשמור את הפוסט לפני שתוכל להעלות קבצים מצורפים חובה לבחור קטגוריה עליך לבחור בעלים אחד לפחות 