��    <      �      �      �     �  ,   �                ,     8     K     d     l     x     �  
   �  
   �  
   �     �     �  &   �     �     �       	   %     /     =     C  ,   b     �     �  
   �     �     �     �       !        @     V     o     u     �     �     �     �     �     �     	       �   !      �  /   �  "   �          1     H     \     r  !   �  *   �     �     �     	  Y  5	     �  P   �     �            -   -  -   [     �     �     �     �     �     �       
        "  8   +     d  .   z  "   �     �  '   �  
     ;     ?   K  &   �      �     �     �  "     .   .  *   ]  /   �  #   �  $   �             %   /  4   U  %   �  
   �     �  +   �     �       �   &  -   �  Q   �  0   B     s     �     �     �  )   �  '   �  0        E     d  "   |   Add due date Add private task lists to your Customer Area Add task Added on %s Assigned to Back to task lists Check tasks in own lists Content Create more Create task list Create/Edit lists Created By Created by Created on Details Done Enter the new task description here... List details Manage tasks of own lists My task lists Next step No task lists Owner Page to create new task lists. Page to list the task lists a customer owns. Recent Task Lists Save and add tasks Task Lists Task lists - New Task lists - Update Task lists published by %1$s Task lists published in %1$s Task lists published in %2$s %1$s Task lists under %1$s Task lists you published Tasks The list does not exist The task list has been created. The title cannot be empty There are no tasks in this list Title View all View any task list View it View task lists You are not allowed to pick an owner and there are no default owners for this type of content. Please contact your website administrator! You currently have no tasklists. You must save the post before you can add tasks You must select at least one owner cuar_taskAdd New cuar_taskAdd New Task cuar_taskView Task cuar_tasklistAdd New cuar_tasklistAdd New Task List cuar_tasklistNo task lists found cuar_tasklistNo task lists found in Trash cuar_tasklistSearch Task Lists cuar_tasklistTask Lists cuar_tasklistView Task List Project-Id-Version: WP Customer Area - Tasks 4.2.0
Report-Msgid-Bugs-To: http://github.com/marvinlabs/customer-area-build-environment/issues
POT-Creation-Date: 2018-04-26 14:24:13+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-08-11 07:35+0000
Last-Translator: ויז׳ן אנד ביונד <yoni@shtik.co.il>
Language-Team: Hebrew
Language: he_IL
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Country: United States
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-Basepath: ../
X-Poedit-SearchPath-0: .
X-Poedit-Bookmarks: 
X-Textdomain-Support: yes
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.0; wp-5.2.2 הוסף תאריך יעד הוסף רשימות משימות פרטיות לאזור הלקוחות שלך הוסף משימה נוסף ב- %s הוקצה ל חזרה לעמוד רשימות משימות בדוק משימות ברשימות משלך תוכן צור עוד משימות צור רשימת משימות צור / ערוך רשימות נוצר ע״י נוצר ע״י נוצר ב פרטים בוצע הזן כאן את תיאור המשימה החדשה... רשימת פרטים נהל משימות של רשימות משלך רשימות המשימות שלי הצעד הבא לא נמצאה רשימת משימות בעלים עמוד ליצירת רשימות משימות חדשות. עמוד לרשימת המשימות שרשומות הלקוח. רשימת משימות אחרונות שמור והוסף משימות רשימת משימות רשימות משימות - jsa רשימת משימות - עדכן רשימת משימות נוצרה ע״י %1$s רשימת משימות נוצרה ב %1$s רשימת משימות נוצרה ב %2$s %1$s רשימת משימות תחת %1$s רשימות משימות שיצרת משימות הרשימה אינה קיימת רשימת המשימות נוצרה. הכותרת אינה יכולה להיות ריקה אין משימות ברשימה זו כותרת הצג הכל הצגת רשימת משימות כלשהי הצג משימה הצג רשימת משימות אינך מורשה לבחור בעלים ואין בעלי ברירת מחדל לסוג תוכן זה. אנא צור קשר עם מנהל האתר שלך! כרגע אין לך רשימת משימות. עליך לשמור את הפוסט לפני שתוכל להוסיף משימות עליך לבחור בעלים אחד לפחות הוסף חדש הוסף משימה חדשה הצג משימה הוסף חדש הוסף רשימת משימות חדשה לא נמצאו רשימת משימות לא נמצאו רשימת משימות בזבל חפש רשימת משימות רשימת משימות צפה ברשימת המשימות 