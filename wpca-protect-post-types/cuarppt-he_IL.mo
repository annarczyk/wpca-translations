��    	      d       �       �      �   7   �   !   �           &     ;  �   X       f  (     �  D   �  I   �  !   ,  "   N  5   q  !  �     �   Assigned to Enable content access restriction with WP Customer Area Make 3rd-party post types private Protect post types Protected post types Public - Anyone can see this Select all the custom post types you would like to protect from public access. Then for each of these items you will be able to decide if you want to assign an owner to it or leave it public Types to protect Project-Id-Version: WP Customer Area - Protect post types 2.1.0
Report-Msgid-Bugs-To: http://github.com/marvinlabs/customer-area-build-environment/issues
POT-Creation-Date: 2018-04-26 14:24:12+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-08-11 09:05+0000
Last-Translator: ויז׳ן אנד ביונד <yoni@shtik.co.il>
Language-Team: Hebrew
Language: he_IL
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Country: United States
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-Basepath: ../
X-Poedit-SearchPath-0: .
X-Poedit-Bookmarks: 
X-Textdomain-Support: yes
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.0; wp-5.2.2 הוקצה ל אפשר הגבלת גישה לתוכן של אזור הלקוחות הפוך את סוגי הפוסטים של צד שלישי לפרטיים הגן על סוגי פוסטים סוגי פוסטים מוגנים ציבורי - כל אחד יכול לראות זאת בחר את כל סוגי הפוסטים המותאמים אישית שתרצה להגן עליהם מגישה ציבורית. ואז עבור כל אחד מהפריטים האלה תוכל להחליט אם ברצונך להקצות לו בעלים או להשאיר אותו ציבורי סוגים להגנה 