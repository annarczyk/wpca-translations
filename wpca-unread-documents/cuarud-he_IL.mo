��          T       �       �      �   "   �   ;   �   7   �      3     D  d  L     �  $   �  B   �  B   0     s  
   �   Mark as unread Mark this document as not read yet Shows for each users which documents have not been read yet This document has been updated since you last opened it Unread Documents Updated Project-Id-Version: WP Customer Area - Unread documents 1.0.1
Report-Msgid-Bugs-To: http://github.com/marvinlabs/customer-area-build-environment/issues
POT-Creation-Date: 2018-04-26 14:24:14+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-08-11 09:02+0000
Last-Translator: ויז׳ן אנד ביונד <yoni@shtik.co.il>
Language-Team: Hebrew
Language: he_IL
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Country: United States
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-Basepath: ../
X-Poedit-SearchPath-0: .
X-Poedit-Bookmarks: 
X-Textdomain-Support: yes
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.0; wp-5.2.2 סמן כלא נקרא סמן מסמך זה כטרם קרא מראה לכל משתמש אילו מסמכים טרם נקראו מסמך זה עודכן מאז שפתחת אותו לאחרונה קבצים שלא נקראו עודכן 