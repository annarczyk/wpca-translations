��    #      4      L      L     M     `     w     ~     �     �     �     �  #   �     �       :        N     V     ]  7   b     �  &   �  
   �     �  #   �       $   ;     `  "   x     �     �  $   �  -   �  "   -  "   P     s     �     �  j  �  &   5	      \	  	   }	     �	     �	     �	     �	  "   �	  6   
  $   =
     b
  r   o
     �
  	   �
  
     \     +   p  5   �     �     �  /     (   8  &   a     �  +   �  $   �  "   �  +     6   8  %   o  "   �     �     �  "   �   Access user groups Additional owner types All %s Any registered user Back-office Choose groups Create/Edit user groups Delete user groups Edit the groups from a user profile Edit user groups Global Make your private content visible to user groups and roles Members New %s Role Select users (hint: you can also type to search a user) This group now has %d member(s) This user does not belong to any group User Group User Groups View the groups from a user profile You do not belong to any group You have not yet created any groups. cuar_user_groupAdd New cuar_user_groupAdd New User Group cuar_user_groupEdit User Group cuar_user_groupNew User Group cuar_user_groupNo user groups found cuar_user_groupNo user groups found in Trash cuar_user_groupParent User Group: cuar_user_groupSearch User Groups cuar_user_groupUser Group cuar_user_groupUser Groups cuar_user_groupView User Group Project-Id-Version: WP Customer Area - Additional Owner Types 5.1.0
Report-Msgid-Bugs-To: http://github.com/marvinlabs/customer-area-build-environment/issues
POT-Creation-Date: 2018-04-26 14:24:09+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-08-10 12:22+0000
Last-Translator: ויז׳ן אנד ביונד <yoni@shtik.co.il>
Language-Team: Hebrew
Language: he_IL
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Country: United States
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-Basepath: ../
X-Poedit-SearchPath-0: .
X-Poedit-Bookmarks: 
X-Textdomain-Support: yes
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.0; wp-5.2.2 גישה לקבוצות משתמשים סוגי בעלים נוספים הכל %s כל משתמש רשום בק-אופיס בחר קבוצות צור / ערוך קבוצות מחק קבוצות משתמשים ערוך את הקבוצות מפרופיל משתמש ערוך קבוצת המשתמשים גלובלי הפוך את התוכן הפרטי שלך גלוי לקבוצות ותפקידים של משתמשים שונים משתמשים רשומים חדש %s תפקיד בחר משתמשים (רמז: אתה יכול גם להקליד כדי לחפש משתמש) בקבוצה זו יש כעת %d חברים משתמש זה אינו שייך לשום קבוצה קבוצת משתמשים קבוצות משתמשים הצג קבוצות מפרופיל המשתמש אתה לא שייך לשום קבוצה עדיין לא יצרת קבוצות. הוסף חדש הוסף קבוצת משתמשים חדשה ערוך קבוצת המשתמשים קבוצת משתמשים חדשה לא נמצאו קבוצות משתמשים לא נמצאו קבוצות משתמשים באשפה קבוצת משתמשים ראשית: חפש קבוצות משתמשים קבוצת משתמשים קבוצות משתמשים צפה בקבוצת משתמשים 