# (c) Vincent Mimoun-Prat, MarvinLabs {2018}
msgid ""
msgstr ""
"Project-Id-Version: WP Customer Area - Owner Restriction 4.1.0\n"
"Report-Msgid-Bugs-To: http://github.com/marvinlabs/customer-area-build-"
"environment/issues\n"
"POT-Creation-Date: 2018-04-26 14:24:11+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2019-08-10 08:35+0000\n"
"Last-Translator: ויז׳ן אנד ביונד <yoni@shtik.co.il>\n"
"Language-Team: \n"
"Language: he_IL\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-Country: United States\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;"
"_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;"
"esc_html_x:1,2c;\n"
"X-Poedit-Basepath: ../\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-Bookmarks: \n"
"X-Textdomain-Support: yes\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.3.0; wp-5.2.2"

#: src/php/owner-restriction-addon.class.php:25
msgid "Owner Restrictions"
msgstr ""

#: src/php/owner-restriction-addon.class.php:101
msgid "Any user"
msgstr ""

#: src/php/owner-restriction-addon.class.php:102
msgid "No user at all"
msgstr ""

#: src/php/owner-restriction-addon.class.php:103
msgid "Only himself"
msgstr ""

#: src/php/owner-restriction-addon.class.php:110
msgid "Any member of the groups where the author is a manager"
msgstr ""

#: src/php/owner-restriction-addon.class.php:111
msgid "Any manager of the groups where the author is a member"
msgstr ""

#: src/php/owner-restriction-addon.class.php:123
msgid "Any users with the role: %s"
msgstr ""

#: src/php/owner-restriction-addon.class.php:134
msgid "Any global rule"
msgstr ""

#: src/php/owner-restriction-addon.class.php:135
msgid "No global rule"
msgstr ""

#: src/php/owner-restriction-addon.class.php:147
msgid "Any role"
msgstr ""

#: src/php/owner-restriction-addon.class.php:148
msgid "No role"
msgstr ""

#: src/php/owner-restriction-addon.class.php:161
msgid "Only the %s role"
msgstr ""

#: src/php/owner-restriction-addon.class.php:172
msgid "Any user group"
msgstr ""

#: src/php/owner-restriction-addon.class.php:173
msgid "No user group"
msgstr ""

#: src/php/owner-restriction-addon.class.php:174
#: src/php/owner-restriction-addon.class.php:190
#: src/php/owner-restriction-addon.class.php:204
msgid "Groups where the author is a member"
msgstr ""

#: src/php/owner-restriction-addon.class.php:186
#: src/php/owner-restriction-addon.class.php:202
msgid "Any group"
msgstr ""

#: src/php/owner-restriction-addon.class.php:187
#: src/php/owner-restriction-addon.class.php:203
msgid "No group"
msgstr ""

#: src/php/owner-restriction-addon.class.php:188
msgid "Groups where the author is a member or a manager"
msgstr ""

#: src/php/owner-restriction-addon.class.php:189
msgid "Groups where the author is a manager"
msgstr ""

#: src/php/owner-restriction-addon.class.php:216
msgid "Any project"
msgstr ""

#: src/php/owner-restriction-addon.class.php:217
msgid "No project"
msgstr ""

#: src/php/owner-restriction-addon.class.php:218
msgid "Projects where the author is involved"
msgstr ""

#: src/php/owner-restriction-addon.class.php:228
msgid "Projects where the author is &laquo; %1$s &raquo;"
msgstr ""

#: src/php/owner-restriction-admin-interface.class.php:36
msgid "Owner Restriction"
msgstr ""

#: src/php/owner-restriction-admin-interface.class.php:48
msgid ""
"The table below sets the owners that can be chosen by an author when "
"creating or editing this private content type."
msgstr ""

#: src/php/templates/restrictions-table.template.php:36
msgid "can create private content for:"
msgstr ""

#. Description of the plugin/theme
msgid "Restrict which owners can be chosen by users when they publish content"
msgstr ""
