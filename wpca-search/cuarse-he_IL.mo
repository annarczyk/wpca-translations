��          �       L      L     M  	   V     `  
   m     x     �     �     �  $   �     �     �     �            B   $     g  
   o     z  Z  �     �     �     �           	          ;  (   M  *   v     �     �     �     �     �  ]   �     T     d  
   v   Any type Ascending Content type Descending Last modification Max. results No match Published on %s, by %s, for %s Published on %s, by yourself, for %s Publishing date Query Search Search result Search something ... Sorry, we could not find any private content matching your search. Sort by Sort order Title Project-Id-Version: WP Customer Area - Search 3.0.3
Report-Msgid-Bugs-To: http://github.com/marvinlabs/customer-area-build-environment/issues
POT-Creation-Date: 2018-04-26 14:24:12+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-08-10 09:19+0000
Last-Translator: ויז׳ן אנד ביונד <yoni@shtik.co.il>
Language-Team: Hebrew
Language: he_IL
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Country: United States
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-Basepath: ../
X-Poedit-SearchPath-0: .
X-Poedit-Bookmarks: 
X-Textdomain-Support: yes
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.0; wp-5.2.2 כל סוג עולה סוג תוכן יורד שינוי אחרון מקסימום תוצאות אין התאמה פורסם ב %s, ע״י %s, עבור %s פורסם ב %s, על ידך, עבור %s תאריך פרסום שאילתא חפש תוצאות חיפוש חפש משהו... מצטערים, לא מצאנו שום תוכן פרטי התואם את החיפוש שלך. מיין לפי סדר המיון כותרת 