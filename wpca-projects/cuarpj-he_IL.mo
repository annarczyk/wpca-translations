��    L      |      �      �  E   �     #     4     D     L     Y     `     l     {  
   �     �     �     �     �     �     �     �     �     �                         $     *     1     ?     H     O     _     g     p     x  	   �     �     �     �     �     �     �     �     �     �               *     ;     J     S     c     r     �     �     �     �  7   �     �  
                  "  	   +     5     :     X     v     |     �     �     �     �     �     �     �     �  \  �  d   S     �     �     �     �                  "   )     L     \     o     �     �     �  
   �  
   �     �  
   �     �     �          )     -     6     C     [     s     |     �     �     �     �     �     �     �     �  /   	  
   9     D     U     d     �  "   �  %   �     �     �               7     Q  !   m     �     �     �  \   �     ,     9     O  
   V     a  
   p  
   {     �     �  
   �  	   �     �     �     �               &  &   ,     S   Automatically compute project progress from its task lists completion Back to projects Completion date Content Contributors Create Create more Create project Create/Edit projects Created by Current value:  Deadline Default status Delete projects Delete statuses Description Details Done Draft Due Due date Edit statuses From:  Guest Guests Hide statuses Kick-off Launch Manage statuses Manager Managers Members My projects Next step None Open Page %1$s/%2$s Page to create new projects. People Previous value:  Progress Progress from tasks Project Project completion date Project due date Project progress Project status Projects Projects - Home Projects - New Projects - Update Projects with status: %1$s Projects: %1$s Recent Projects Schedule Select users (hint: you can also type to search a user) Settings Start date Started Status Statuses Suspended Team The project has been created. The project has been updated. Title To:  Update Update project View Projects View all View it Y/m/d You currently have no projects. cuar_projectView Project Project-Id-Version: WP Customer Area - Projects 4.2.0
Report-Msgid-Bugs-To: http://github.com/marvinlabs/customer-area-build-environment/issues
POT-Creation-Date: 2018-04-26 14:24:11+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-08-10 09:59+0000
Last-Translator: ויז׳ן אנד ביונד <yoni@shtik.co.il>
Language-Team: Hebrew
Language: he_IL
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Country: United States
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-Basepath: ../
X-Poedit-SearchPath-0: .
X-Poedit-Bookmarks: 
X-Textdomain-Support: yes
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.0; wp-5.2.2 חשב אוטומטית את התקדמות הפרויקט ע״י רשימות המשימות שלו חזרה לפרויקטים תאריך השלמה תוכן תורמים צור צור עוד צור פרויקט צור / ערוך פרויקטים נוצר ע״י ערך נוכחי: מועד אחרון סטטוס ברירת מחדל מחק פרויקטים מחק סטטוסים תיאור פרטים בוצע טיוטה חל תאריך יעד ערוך סטטוסים מ: אורח אורחים הסתר סטטוסים בעיטת הפתיחה השקה נהל סטטוסים מנהל מנהלים משתמשים הפרויקטים שלי הצעד הבא אף אחד פתוח עמוד %1$s/%2$s עמוד ליצור פרויקטים חדשים אנשים ערך קודם: התקדמות התקדמות ממשימות פרויקט תאריך סיום הפרויקט תאריך יעד של הפרויקט התקדמות פרויקט סטטוס פרויקט פרויקטים פרויקטים - בית פרויקטים - חדש פרויקטים - עדכן סטטוס פרויקטים: %1$s פרויקטים: %1$s פרויקטים אחרונים תזמן בחר משתמשים (רמז: אתה יכול גם להקליד כדי לחפש משתמש) הגדרות תאריך התחלה החל סטטוס סטטוסים מושעה קבוצה הפרויקט נוצר. הפרויקט עודכן. כותרת עבור: עדן עדכן פרויקט הצג פרויקטים הצג הכל הצג פרויקט Y/m/d כרגע אין לך פרויקטים. הצג פרויקט 